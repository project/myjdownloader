My jDownloader
=======================

Package : `myjdownloader`

## About
This is an API module allow to interact with MY.jDownloader API. 
First, you need JDownloader 2 Beta installed on your PC or other hardware,
visit http://jdownloader.org/download/offline.
Setup JDownloader for my.jdownloader.org in Settings/My.JDownloader.

If you already registered at my.jdownloader.org than fill fields Email,
Password and Device Name on settings, otherwise "Go to My.JDownloader.org"
for registration.

Links :
* [My.JDownloader.org](https://my.jdownloader.org/)
* [My.JDownloader API Documentation](https://my.jdownloader.org/developers/)
* [api-php-class](https://github.com/tofika/my.jdownloader.org-api-php-class)
